import { sls } from '../utils/http';

export function loadPublicProperties() {
    return sls.get(`/properties/public`);
}

export function loadProperties() {
    return sls.get(`/properties`);
}

export function getPropertyById(id) {
    return sls.get(`/properties/${id}`);
}

export function deleteProperties(id) {
    return sls.delete(`/properties/${id}`);
}

export function getMessages(email) {
    return sls.get(`/messages?email=${email}`);
}

export function getMessage(id) {
    return sls.get(`/messages/${id}`);
}

export function deleteMessage(id) {
    return sls.delete(`/messages/${id}`);
}

export function updateSharing(update) {
    return sls.post(`/channel/updateSharing`, update);
}

export function presignedURL(config, type) {
    return sls.get(`/s3/presigned-url?type=${type}`, config);
}

export function getUsers() {
    return sls.get(`/users`);
}

export function inviteUser(email, action) {
    return sls.post(`/users/invite`, {email: email, action: action});
}

export function updateUserAttributes(object) {
    return sls.put(`/users`, object);
}

export function changePassword(agentForm) {
    return sls.put(`/users/change-password`, agentForm);
}

export function deleteUser(id) {
    return sls.delete(`/users/${id}`);
}

export function getPendingUsers() {
    return sls.get(`/admin/list`);
}

export function approvePendingUser(id) {
    return sls.post(`/admin/approve/${id}`);
}