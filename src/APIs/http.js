import {http} from '../utils/http';

export function getCollection() {
    return http.get(`/collection`);
}

export function putCollection(collection) {
    return http.put(`/collection`, collection);
}

export function updateProperty(id, property) {
    return http.put(`/properties/${id}`, property);
}

export function postProperty(property) {
    return http.post(`/properties`, property);
}

export function getCompanyById(companyId) {
    if(companyId) return http.get(`/company/${companyId}`);
}

export function getAllCompany() {
    return http.get(`/company/all`);
}

export function updateCompany(id, company) {
    return http.put(`/company/${id}`, company);
}

export function postCompany(company) {
    return http.post(`/company`, company);
}

