import Vue from 'vue'
import App from './App.vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import router from './router/index'
import './utils/element.js'
import './assets/iconfont/iconfont.css'
import VueClipboard from 'vue-clipboard2'
import SocialSharing from 'vue-social-sharing'
import Amplify from 'aws-amplify'
import awsconfig from './aws-exports'
Amplify.configure(awsconfig)


Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAmLtmAnXUomeJe8O-Po_73VXrVR_zboNg',
    libraries: 'places', 
  },
  installComponents: true
})

Vue.use(VueClipboard);
Vue.use(SocialSharing);

import {http, sls} from './utils/http.js'
Vue.prototype.$http = http
Vue.prototype.$sls = sls


new Vue({
  render: h => h(App),
  router
}).$mount('#app')
