import Vue from 'vue'
import VueRouter from 'vue-router'
import AdminHome from '@/views/AdminHome'
import AgentList from '@/views/agent/AgentList'
import PropertyEdit from '@/views/property/PropertyEdit'
import PropertyView from '@/views/property/PropertyView'
import PropertyList from '@/views/property/PropertyList'
import Profile from '@/views/setting/Profile'
import UserList from '@/views/setting/user/UserList'
import UserEdit from '@/views/setting/user/UserEdit'
import PasswordUpdate from '@/views/setting/user/PasswordUpdate'
import P404 from '@/views/error/404'
import PropertyHomePage from '@/views/home/PropertyList'
import Favorites from '@/views/personnel/Favorites'
import MessageList from '@/views/setting/message/MsgList'
import AccountApprove from '@/views/setting/super-admin/AcctApprove'


Vue.use(VueRouter)
const adminRouter = [

  {
    path: '/',
    name: 'home',
    component: AdminHome,
    children: [
      {path: '/home', component: PropertyHomePage},
      {path: '/properties/create', component: PropertyEdit},
      {path: '/properties/edit/:id', component: PropertyEdit, props: true},
      {path: '/properties/view/:id', component: PropertyView, props: true},
      {path: '/properties/list', component: PropertyList},
      {path: '/channels/list', component: AgentList},
      {path: '/settings/profile', component: Profile},
      {path: '/settings/users', component: UserList},
      {path: '/password/update', component: PasswordUpdate},
      {path: '/favorites', component: Favorites},
      {path: '/users/profile', component: UserEdit},
      {path: '/messages/list', component: MessageList},
      {path: '/super-admin', component:AccountApprove},
    ]
  },
  {path: '/404', name: 'P404', component: P404},
  {path: '*', "redirect": "/404", "hidden": true}
]
// const staffRouter = [
//   {
//     path: '/',
//     name: 'home',
//     component: AdminHome,
//     children: [
//       {path: '/home', component: PropertyHomePage},
//       {path: '/settings/profile', component: Profile},
//       {path: '/password/update', component: PasswordUpdate},
//       {path: '/favorites', component: Favorites},
//       {path: '/users/profile', component: UserEdit},
//       {path: '/messages/list', component: MessageList},
//     ]
//   },
//   {path: '/404', name: 'P404', component: P404},
//   {path: '*', "redirect": "/404", "hidden": true}
// ]

const router = new VueRouter({routes: adminRouter});

export default router;