import axios from 'axios'
import Vue from 'vue'
import {
  Loading
} from 'element-ui';
import {Auth} from "aws-amplify";

const DOMAIN = process.env.VUE_APP_ROOT_API;

export const http = axios.create({
  baseURL: `${DOMAIN}/admin/api/rest/`
})

http.interceptors.request.use(function (config) {
  config.headers.Authorization =  sessionStorage.token;
  config.headers.companyid =  sessionStorage.companyId;
  return config;
});

export const sls = axios.create({
  baseURL: `${DOMAIN}/sls`
})

sls.interceptors.request.use(function (config) {
  config.headers.Authorization =  sessionStorage.token;
  config.headers.companyid =  sessionStorage.companyId;
  return config;
});

http.uploadURL = `${DOMAIN}/admin/api/`;

http.STORAGE_URL = 'https://image.weproperty.com.au/';

http.UTIL_URL = 'https://util.weproperty.com.au/sls/';

let loadingInstance
let count = 0;
http.interceptors.request.use(config => {
  if (count > 0) {
    loadingInstance.close()
  }
  count++
  loadingInstance = Loading.service({
    target: '.content',
    text: 'Loading...',
    fullscreen: false,
  })

  return config;
})

http.interceptors.response.use(res => {
  loadingInstance.close()
  count = 0
  return res;
}, async err => {
  loadingInstance.close()
  count = 0

  Vue.prototype.$message({
    type: 'error',
    message: err.response ? err.response : 'Server error, Please contact admin'
  })

  if (err.response.status === 401) {
    await Auth.signOut()
      .then(() => {
        sessionStorage.clear();
      })
      .catch(err => console.log(err));
  }

  return Promise.reject(err);
});


let slsLoadingInstance;
let slsCount = 0;
sls.interceptors.request.use(config => {
  if (slsCount > 0) {
    slsLoadingInstance.close()
  }
  slsCount++
  slsLoadingInstance = Loading.service({
    target: '.content',
    text: 'Loading...',
    fullscreen: false,
  })
  // if (sessionStorage.token) {
  //     config.headers.Authorization = 'Bearer ' + sessionStorage.token;
  //     config.headers.companyId = sessionStorage.companyId;
  // }
  return config;
})
sls.interceptors.response.use(res => {
  slsLoadingInstance.close()
  slsCount = 0
  return res;
}, async err => {
  slsLoadingInstance.close()
  slsCount = 0

  if (err.response.status === 401) {
    await Auth.signOut()
      .then(() => {
        sessionStorage.clear();
      })
      .catch(err => console.log(err));
  }

  return Promise.reject(err);
});