import {Auth} from "aws-amplify";

export async function isPendingAcct() {
  const session = await Auth.currentSession();
  return session.getIdToken().payload["cognito:groups"][0] == "pending" ? true : false;
}

export async function isAdmin() {
  const user = await Auth.currentAuthenticatedUser();
  return user.attributes["custom:role"] === "Admin"
}

