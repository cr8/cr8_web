# cr8_web

# Design
![Design](1608154802205.jpeg)

#Preview
![Dashboard](mmexport1716955350173.jpg)
![User](mmexport1716955428323.jpg)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
